#include <stdio.h>
#include <stdint.h>

struct card {
    uint8_t count;
    char* name;
    uint32_t color;
    char* image;
    uint8_t action;
};

struct deck {
    struct card* cards;
};


void deck_add_card
