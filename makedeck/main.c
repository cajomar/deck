#include <stdio.h>

#include "../src/client.h" // enum action
#include "../src/serializer.h"
#include "../src/stb_ds.h"

void w_img(struct i_writer* w, const char* path) {
    FILE* fp = fopen(path, "rb");
    if (!fp) {
        printf("Failed to open %s\n", path);
        exit(1);
    }
    fseek(fp, 0, SEEK_END);
    long len = ftell(fp);
    w_u64(w, len);
    rewind(fp);
    uint8_t* buf = arraddnptr(w->data, len);
    fread(buf, len, 1, fp);
    fclose(fp);
}

int main(void) {
    struct i_writer w = { .data = 0 };
    w_str(&w, "The Settlers of Catan (5-6 players)");

    w_u8(&w, 10);

    w_u8(&w, 20);
    w_str(&w, "Soldier");
    w_u32(&w, 0xCD0607FF);
    w_img(&w, "assets/sword.png");
    w_u8(&w, 0);
    w_u8(&w, ACTION_REVEAL);

    w_u8(&w, 5);
    w_str(&w, "Victory Point");
    w_u32(&w, 0x0430CDFF);
    w_img(&w, "assets/church.png");
    w_u8(&w, 0);
    w_u8(&w, ACTION_REVEAL);

    w_u8(&w, 3);
    w_str(&w, "Year of Plenty");
    w_u32(&w, 0x02CF0EFF);
    w_img(&w, "assets/resources.png");
    w_u8(&w, 0);
    w_u8(&w, ACTION_DISCARD);

    w_u8(&w, 3);
    w_str(&w, "Road Building");
    w_u32(&w, 0xD77721FF);
    w_img(&w, "assets/bridge.png");
    w_u8(&w, 0);
    w_u8(&w, ACTION_DISCARD);

    w_u8(&w, 3);
    w_str(&w, "Monopoly");
    w_u32(&w, 0x9302CDFF);
    w_img(&w, "assets/market.png");
    w_u8(&w, 0);
    w_u8(&w, ACTION_DISCARD);

    w_u8(&w, 24);
    w_str(&w, "Brick");
    w_u32(&w, 0xE23300FF);
    w_img(&w, "assets/brick.png");
    w_u8(&w, 1);
    //w_u8(&w, ACTION_DISCARD);
    w_u8(&w, ACTION_TOP_DRAW);

    w_u8(&w, 24);
    w_str(&w, "Wood");
    w_u32(&w, 0x824225FF);
    w_img(&w, "assets/wood.png");
    w_u8(&w, 1);
    //w_u8(&w, ACTION_DISCARD);
    w_u8(&w, ACTION_TOP_DRAW);

    w_u8(&w, 24);
    w_str(&w, "Sheep");
    w_u32(&w, 0xF5EAC0FF);
    w_img(&w, "assets/sheep.png");
    w_u8(&w, 1);
    //w_u8(&w, ACTION_DISCARD);
    w_u8(&w, ACTION_TOP_DRAW);

    w_u8(&w, 24);
    w_str(&w, "Wheat");
    w_u32(&w, 0xFFF74CFF);
    w_img(&w, "assets/wheat.png");
    w_u8(&w, 1);
    //w_u8(&w, ACTION_DISCARD);
    w_u8(&w, ACTION_TOP_DRAW);

    w_u8(&w, 24);
    w_str(&w, "Ore");
    w_u32(&w, 0x7F7474FF);
    w_img(&w, "assets/ore.png");
    w_u8(&w, 1);
    //w_u8(&w, ACTION_DISCARD);
    w_u8(&w, ACTION_TOP_DRAW);

    w_u8(&w, 2);

    w_str(&w, "Development");
    w_u32(&w, 0xEE8DFFFF);
    w_img(&w, "data/deck1.png");
    w_u8(&w, 0);
    w_u8(&w, 21);
    //w_u8(&w, 1);

    w_str(&w, "Resource");
    w_u32(&w, 0x3895F1FF);
    w_img(&w, "data/deck2.png");
    w_u8(&w, 1);
    w_u8(&w, 23);
    //w_u8(&w, 0);

    FILE* fp = fopen("packs/out.pack", "wb");
    fwrite(w.data, arrlenu(w.data), 1, fp);
    fclose(fp);

    arrfree(w.data);


    return 0;
}
