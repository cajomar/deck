import os
import stat

import tempita


namespace = {
        "packs": [],
        }


class Pack:
    def __init__(self, name):
        with open(name, "rb") as f:
            a = f.read()

        self.data = ''
        i = 0
        pl = 80 // 4
        while i < len(a):
            self.data += ','.join([str(b) for b in a[i:min(i + pl, len(a))]])
            self.data += ',\n'
            i += pl

        namespace["packs"].append(self)


for f in os.listdir("packs"):
    Pack(os.path.join("packs", f))


def do_file(input_filename, output_filename):
    tmpl = tempita.Template.from_filename(
            input_filename,
            namespace=namespace,
            encoding="utf8")
    result = tmpl.substitute().encode("utf8")

    if os.path.exists(output_filename):
        os.chmod(output_filename, stat.S_IREAD | stat.S_IWRITE)
    try:
        with open(output_filename, "rb") as f:
            changed = f.read() != result
    except FileNotFoundError:
        changed = True
    if changed:
        with open(output_filename, "wb") as f:
            f.write(result)

    with open(output_filename, "wb") as f:
        f.write(result)
    os.chmod(output_filename, stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH)


if __name__ == "__main__":
    do_file("src/pack_loader.c.in", "src/pack_loader.c")
