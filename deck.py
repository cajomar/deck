import enum
import struct


class ACTION(enum.IntEnum):
    DISCARD = 0
    REVEAL = 1
    TOP_DRAW = 2
    BOTTOM_DRAW = 3


class REVEAL(enum.IntFlag):
    IN_HAND = 1
    IN_DECK_DRAW = 2
    IN_DECK_PLAYED = 4
    IN_OPPONENT_DRAW = 8
    IN_OPPONENT_PLAYED = 16
    IN_DRAW = 11
    WHEN_PLAYED = 21


class Card:
    def __init__(self, name, count, color, image, action=ACTION.DISCARD):
        self.name = name
        self.count = count
        self.color = color
        self.action = action
        self.image = image


class Deck:
    def __init__(self, name, color, image, visibility=REVEAL.WHEN_PLAYED, group=False):
        self.name = name
        self.color = color
        self.image = image
        self.visibility = visibility
        self.group = group
        self.cards = []

    def add_card(self, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Card):
            self.cards.append(args[0])
            return args[0]
        else:
            c = Card(*args, **kwargs)
            self.cards.append(c)
            return c


class Writer:
    def __init__(self):
        self.data = bytes()

    def __getattr__(self, function):
        m = {"8": "b", "16": "h", "32": "i", "64": "q"}
        form = m[function[1:]]
        if function[0] == "u":
            form = form.upper()

        def fun(v):
            self.data += struct.pack("="+form, v)

        return fun

    def str(self, s):
        s = bytes(s, "utf8")
        self.i32(len(s))
        self.data += s

    def img(self, s):
        f = open(s, "rb")
        d = f.read()
        f.close()
        self.u64(len(d))
        self.data += d

    def write(self, path):
        f = open(path, "wb")
        f.write(self.data)
        f.close()


class Pack:
    def __init__(self, name):
        self.name = name
        self.decks = []

    def add_deck(self, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Card):
            self.decks.append(args[0])
            return args[0]
        else:
            d = Deck(*args, **kwargs)
            self.decks.append(d)
            return d

    def write(self, name):
        w = Writer()
        w.str(self.name)
        w.u8(sum([len(d.cards) for d in self.decks]))

        for i, d in enumerate(self.decks):
            for c in d.cards:
                w.u8(c.count)
                w.str(c.name)
                w.u32(c.color)
                w.img(c.image)
                w.u8(i)
                w.u8(c.action)

        w.u8(len(self.decks))
        for d in self.decks:
            w.str(d.name)
            w.u32(d.color)
            w.img(d.image)
            w.u8(1 if d.group else 0)
            w.u8(d.visibility)

        w.write(name)


p = Pack("The Settlers of Catan (5-6 players)")

dev = p.add_deck("Development", 0xEE8DFFFF, "data/deck1.png")
dev.add_card("Soldier", 20, 0xCD0607FF, "assets/sword.png", action=ACTION.REVEAL)
dev.add_card("Victory Point", 5, 0x0430CDFF, "assets/church.png", action=ACTION.REVEAL)
dev.add_card("Year of Plenty", 3, 0x02CF0EFF, "assets/resources.png")
dev.add_card("Road Building", 3, 0xD77721FF, "assets/bridge.png")
dev.add_card("Monopoly", 3, 0x9302CDFF, "assets/market.png")

res = p.add_deck("Resource", 0x3895F1FF, "data/deck2.png", group=True, visibility=REVEAL.IN_DRAW)
res.add_card("Brick", 24, 0xE23300FF, "assets/brick.png", action=ACTION.TOP_DRAW)
res.add_card("Wood", 24, 0x824225FF, "assets/wood.png", action=ACTION.TOP_DRAW)
res.add_card("Sheep", 24, 0xF5EAC0FF, "assets/sheep.png", action=ACTION.TOP_DRAW)
res.add_card("Wheat", 24, 0xFFF74CFF, "assets/wheat.png", action=ACTION.TOP_DRAW)
res.add_card("Ore", 24, 0x7F7474FF, "assets/ore.png", action=ACTION.TOP_DRAW)

p.write("packs/out.pack")
