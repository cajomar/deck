import os
import stat
import enum

import tempita


class ACTION(enum.IntEnum):
    DISCARD = enum.auto()
    REVEAL = enum.auto()


class REVEAL(enum.IntFlag):
    IN_HAND = enum.auto()
    IN_DECK_DRAW = enum.auto()
    IN_DECK_PLAYED = enum.auto()
    IN_OPPONENT_DRAW = enum.auto()
    IN_OPPONENT_PLAYED = enum.auto()

    IN_DRAW = IN_HAND | IN_DECK_DRAW | IN_OPPONENT_DRAW
    WHEN_PLAYED = IN_HAND | IN_DECK_PLAYED | IN_OPPONENT_PLAYED


namespace = {
    "decks": [],
    "card_types": [],
    "ACTION": ACTION,
    "REVEAL": REVEAL,
    }


class Deck:
    def __init__(self, name, color, visibility=REVEAL.WHEN_PLAYED,
                 group=False, has_discard=True):
        self.name = name
        self.color = color
        self.visibility = visibility
        self.group = group
        self.has_discard = has_discard
        namespace["decks"].append(self)


class CardType:
    def __init__(self, name, deck, count, color, action=ACTION.DISCARD):
        self.name = name
        self.color = color
        self.deck = [d.name for d in namespace["decks"]].index(deck)
        self.action = action
        self.count = count
        namespace["card_types"].append(self)


Deck("Development", 0xEE8DFFFF)
CardType("Soldier", "Development", 20, 0xCD0607FF, action=ACTION.REVEAL)
CardType("Victory Point", "Development", 5, 0x0430CDFF, action=ACTION.REVEAL)
CardType("Year of Plenty", "Development", 3, 0x02CF0EFF)
CardType("Road Building", "Development", 3, 0xD77721FF)
CardType("Monopoly", "Development", 3, 0x9302CDFF)
Deck("Resource",
     0x3895F1FF,
     visibility=REVEAL.WHEN_PLAYED | REVEAL.IN_DECK_DRAW,
     group=True,
     has_discard=False)
CardType("Brick", "Resource", 24, 0xE23300FF)
CardType("Wood", "Resource", 24, 0x824225FF)
CardType("Sheep", "Resource", 24, 0xF5EAC0FF)
CardType("Wheat", "Resource", 24, 0xFFF74CFF)
CardType("Ore", "Resource", 24, 0x7F7474FF)

namespace["card_types"].sort(key=lambda t: t.deck)


def do_file(input_filename, output_filename):
    tmpl = tempita.Template.from_filename(
            input_filename,
            namespace=namespace,
            encoding="utf8")
    result = tmpl.substitute().encode("utf8")

    if os.path.exists(output_filename):
        os.chmod(output_filename, stat.S_IREAD | stat.S_IWRITE)
    try:
        with open(output_filename, "rb") as f:
            changed = f.read() != result
    except FileNotFoundError:
        changed = True
    if changed:
        with open(output_filename, "wb") as f:
            f.write(result)
    os.chmod(output_filename, stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH)


if __name__ == "__main__":
    do_file("src/gen.h.in", "src/gen.h")
    do_file("src/gen.c.in", "src/gen.c")
