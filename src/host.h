#ifndef DECK_HOST_H_O2RUTARF
#define DECK_HOST_H_O2RUTARF


#include "serializer.h"
#include "enet.h"

#include <ink_data.h>


#define NET_PORT 8901
#define MAX_PLAYERS 32
#define BANK_HAND 255

typedef struct card {
    uint8_t type;
    uint8_t hand;
    bool played;
    int32_t order;
} Card;

typedef struct player {
    char* name;
    bool connected;
} Player;

typedef struct host {
    ENetAddress address;
    ENetHost* server;
    struct i_writer cmd_writer;
    Card* cards; // stretchy_buffer
    Player* players; // stretchy_buffer
    char pack_file_name[128];
} Host;


Host* host_create(void);
void host_destroy(Host* host);
void host_start_game(Host* host);
void host_step(Host* host);


#endif /* end of include guard: DECK_HOST_H_O2RUTARF */
