#ifndef STB_DS_LOOP_H_BSUAK5MT
#define STB_DS_LOOP_H_BSUAK5MT

#include "stb_ds.h"

// foreach, with a pointer into the array
#define arrforeachp(v, a) \
    for (int arr_counter=0, arr__loop_break_outer=false; \
            arr_counter<arrlen((a)) /* counter */ \
            && !arr__loop_break_outer /* only continue if the inner loop didn't hit a break statement */ \
            && (arr__loop_break_outer = true); /* set the variable so we can track if the inner loop completed without a break */ \
            arr_counter++) \
        for (v = &(a)[arr_counter]; /* inner loop is executed once, so that we can initialize a variable */ \
                arr__loop_break_outer;\
                arr__loop_break_outer = false /* signal that we've finished an iteration without breaking */)
// foreach, copying the values of the array
#define arrforeachv(v, a) \
    for (int arr_counter=0, arr__loop_break_outer=false; \
            arr_counter<arrlen((a)) /* counter */ \
            && !arr__loop_break_outer /* only continue if the inner loop didn't hit a break statement */ \
            && (arr__loop_break_outer = true); /* set the variable so we can track if the inner loop completed without a break */ \
            arr_counter++) \
        for (v = (a)[arr_counter]; /* inner loop is executed once, so that we can initialize a variable */ \
                arr__loop_break_outer;\
                arr__loop_break_outer = false /* signal that we've finished an iteration without breaking */)

#endif /* STB_DS_LOOP_H_BSUAK5MT */
