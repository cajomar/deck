#include <stdio.h>

#include "../host.h"


int main(void) {
    Host* host = host_create();
    printf("Created host.\n");

    while (true) {
        host_step(host);
        usleep(20000);
    }

    return 0;
}
