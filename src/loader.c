#include "loader.h"
#include "serializer.h"
#include "stb_ds_loop.h"

Card* loader_deserialize(const uint8_t** r) {
    Card* cards = 0; // stretchy_buffer
    uint16_t s = r_u16(r);
    arrsetlen(cards, s);
    arrforeachp(Card* c, cards) {
        c->type = r_u8(r);
        c->hand = r_u8(r);
        c->played = r_u8(r);
        c->order = r_i32(r);
    }
    return cards;
}
