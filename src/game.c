#include "game.h"

#include <assert.h>

#include <ink.h>
#include <ink_data.h>

#include "client.h"
#include "enet.h"
#include "host.h"
#include "idents.h"


Game* game_new(struct ink* ink) {
    if (enet_initialize() != 0) {
        LOGE("Failed to initalize ENet.\n");
    }
    Game* g = calloc(1, sizeof(Game));
    g->ink = ink;
    g->listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_main);
    inkd_add_listener(source, g->listener);

    const char* name = getenv("USER");
    if (!name) name = getenv("USERNAME");
    if (!name) name = "caleb";
    if (name) inkd_charp(source, I_name, name);

    return g;
}

void game_destroy(Game* g) {
    client_destroy(g->client);
    host_destroy(g->host);
    inkd_listener_delete(g->listener);
    free(g);
}

void game_step(Game* g) {
    if (!g) return;
    struct ink_data_event ev;
    while (inkd_listener_poll(g->listener, &ev)) {
        switch (ev.type) {
        case INK_DATA_EVENT_CHANGE:
            switch (ev.field) {
            case I_client:
                if (inkd_get_bool(ev.data, ev.field)) {
                    assert(!g->client);
                    g->client = client_create(g->ink,
                        inkd_get_charp(ev.data, I_connect_address), 
                        inkd_get_charp(ev.data, I_name));
                } else {
                    assert(g->client);
                    client_destroy(g->client);
                    g->client = 0;
                }
                break;
            case I_host:
                if (inkd_get_bool(ev.data, ev.field)) {
                    assert(!g->host && !g->client);
                    g->host = host_create();
                    g->client = client_create(g->ink, "127.0.0.1", inkd_get_charp(ev.data, I_name));
                } else {
                    assert(g->host && g->client);
                    host_destroy(g->host);
                    g->host = 0;
                    client_destroy(g->client);
                    g->client = 0;
                }
                break;
            }
            break;
        default:
            break;
        }
    }
    host_step(g->host);
    client_step(g->client, g->ink);
}
