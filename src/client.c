#include "client.h"

#include "host.h"
#include "game.h" // for LOGE
#include "idents.h"
#include "loader.h"
#include "serializer.h"

#include "stb_ds_loop.h"

#include <ink_query.h>
#include <ink_experimental.h>

#include <assert.h>
#include <string.h>

#define I_order ink_ident_hash("order")

static void send_client_info(Client* client) {
    ENetPacket* packet = enet_packet_create(NULL,
            1 + strlen(client->player_name)+1,
            ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_player_name;
    strcpy((char*)packet->data+1, client->player_name);
    enet_peer_send(client->peer, 0, packet);
}


Client* client_create(struct ink* ink, const char* host, const char* name) {
    LOGD("Initializing client. Host: %s; name: %s\n", host, name);

    Client* client = calloc(1, sizeof(Client));

    client->player_name = strdup(name);
    client->host_address = strdup(host);

    client->client = enet_host_create(NULL, 1, 2, 0, 0);
    if (client->client == 0) {
        LOGE("Error: failed to create ENet client.\n");
    }

    ENetAddress address;
    enet_address_set_host(&address, host);
    address.port = NET_PORT;
    client->peer = enet_host_connect(client->client, &address, 2, 0);
    if (client->peer == 0) {
        LOGE("Error: failed to create ENet peer.\n");
    }
    inkd source = ink_source(ink, I_game);
    inkd_clear_all(source);
    client->listener = inkd_listener_new(0);
    inkd_add_listener(source, client->listener);

    inkd_int(source, I_bank, BANK_HAND);
    return client;
}

void client_destroy(Client* client) {
    if (!client) return;
    enet_peer_disconnect(client->peer, 0);
    ENetEvent event;
    while (enet_host_service(client->client, &event, 0) > 0) {}
    enet_host_destroy(client->client);
    free(client->player_name);
    free(client->host_address);
    inkd_listener_delete(client->listener);
    arrfree(client->cards);
    free(client);
}

void client_reconnect(Client* client) {
    assert(client);
    LOGD("Client: trying to reconnect to %s\n", client->host_address);

    enet_peer_disconnect(client->peer, 0);
    ENetEvent event;
    while (enet_host_service(client->client, &event, 0) > 0) {}
    enet_host_destroy(client->client);
    
    client->client = enet_host_create(NULL, 1, 2, 0, 0);
    if (client->client == 0) {
        LOGE("Error: failed to create ENet client.\n");
    }
    ENetAddress address;
    enet_address_set_host(&address, client->host_address);
    address.port = NET_PORT;
    client->peer = enet_host_connect(client->client, &address, 2, 0);
    if (client->peer == 0) {
        LOGE("Error: failed to create ENet peer.\n");
    }
}

static void 
send_card_change(ENetPeer* peer, uint16_t card, uint8_t hand, bool is_played, int32_t order) {
    struct i_writer w  = { .data = 0 };
    w_u16(&w, card);
    w_u8(&w, hand);
    w_u8(&w, is_played);
    w_i32(&w, order);
    ENetPacket* packet = enet_packet_create(NULL,
            1 + arrlen(w.data),
            ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_card_change;
    memcpy(packet->data+1, w.data, arrlen(w.data));
    arrfree(w.data);
    enet_peer_send(peer, 0, packet);
}

static uint8_t get_card_reveal_flags(uint8_t player_id, 
        uint8_t card_player_id, 
        bool played) {
    uint8_t ret = 0;
    if (card_player_id == player_id) {
        ret |= REVEAL_IN_HAND;
    } else if (card_player_id == BANK_HAND) {
        if (played) {
            ret |= REVEAL_IN_DECK_PLAYED;
        } else {
            ret |= REVEAL_IN_DECK_DRAW;
        }
    } else {
        if (played) {
            ret |= REVEAL_IN_OPPONENT_PLAYED;
        } else {
            ret |= REVEAL_IN_OPPONENT_DRAW;
        }
    }
    return ret;
}

static void update_cards(Client* client, struct ink* ink, Card* cards) {
    inkd source = ink_source(ink, I_game);
    inkd_rebuild_begin(source, I_cards);
    ink_query q;
    ink_query_init(&q);
    arrsetlen(client->cards, arrlen(cards));
    arrforeachp(Card* c, cards) {
        ink_query_int(&q, I_id, arr_counter);
        inkd d = ink_query_get(&q, source, I_cards);
        inkd_int(d, I_type, c->type+1);
        inkd_int(d, I_hand, c->hand);
        inkd_int(d, I_order, c->order);
        inkd_bool(d, I_played, c->played);
        if (!inkd_get_int(d, I_slot)) {
            inkd_int(d, I_slot, 1);
        }
        client->cards[arr_counter] = d;
    }
    ink_query_deinit(&q);
    inkd_rebuild_end(source, I_cards);
}

static void load_image(struct ink* ink, inkd source, const uint8_t** r) {
    struct ink_resources* res = ink_get_resources(ink);
    uint64_t len = r_u64(r);
    struct ink_image* img = ink_resources_load_image_from_memory(res, *r, len, 0);
    inkd_image(source, I_image, img);
    *r += len;
}

static void 
load_pack(Client* client, struct ink* ink, const uint8_t* data) {
    inkd source = ink_source(ink, I_game);
    inkd_clear(source, I_card_types);
    inkd_clear(source, I_decks);

    const uint8_t* r = data;
    // Skip game name
    //r += r_i32(&r);
    char* game_name = r_str(&r);
    LOGD("Loading pack '%s'\n", game_name);
    free(game_name);

    uint8_t num_card_types = r_u8(&r);

    arrsetlen(client->card_types, num_card_types);

    arrforeachp(CardType* ct, client->card_types) {
        inkd d = inkd_append(source, I_card_types);
        inkd_int(d, I_type, arr_counter + 1); // ink lists are 1-based

        r += 1; // Skip num cards

        char* name = r_str(&r);
        inkd_charp(d, I_name, name);
        free(name);
        inkd_color(d, I_color, r_u32(&r));

        load_image(ink, d, &r);

        ct->deck = r_u8(&r);
        inkd_int(d, I_deck, ct->deck + 1); // ink lists are 1-based
        ct->action = r_u8(&r);
        
    }

    uint8_t num_decks = r_u8(&r);
    arrsetlen(client->deck_types, num_decks);

    arrforeachp(DeckType* dt, client->deck_types) {
        inkd d = inkd_append(source, I_decks);
        inkd_int(d, I_decks, arr_counter + 1);
        
        char* name = r_str(&r);
        inkd_charp(d, I_name, name);
        free(name);

        inkd_color(d, I_color, r_u32(&r));

        load_image(ink, d, &r);

        inkd_bool(d, I_group, r_u8(&r));
        dt->visibility = r_u8(&r);
        //dt->has_discard = r_u8(&r);
    }
}



void client_step(Client* client, struct ink* ink) {
    if (!client) return;
    inkd source = ink_source(ink, I_game);
    ENetEvent event;
    while (enet_host_service(client->client, &event, 0) > 0) {
        switch (event.type) {
        case ENET_EVENT_TYPE_CONNECT:
            LOGD("Client: connected to server.\n");
            send_client_info(client);
            break;
        case ENET_EVENT_TYPE_RECEIVE: {
            const uint8_t* data = (event.packet->data)+1;
            switch ((enum message_type) event.packet->data[0]) {
            case MSG_cards: {
                LOGD("Client: Recieved cards\n");
                const uint8_t* r = data;
                Card* cards = loader_deserialize(&r);
                update_cards(client, ink, cards);
                arrfree(cards);
                break;
            }
            case MSG_player_id:
                LOGD("Client: Recived player id %d\n", data[0]);
                inkd_int(source, I_player_id, data[0]);
                break;
            case MSG_pack:
                LOGD("Client: Recived pack.\n");
                load_pack(client, ink, data);
                break;
            case MSG_card_change: {
                const uint8_t* r = data;
                uint16_t c = r_u16(&r);
                uint8_t h = r_u8(&r);
                bool p = r_u8(&r);
                int32_t o = r_i32(&r);
                inkd_int(client->cards[c], I_hand, h);
                inkd_bool(client->cards[c], I_played, p);
                inkd_int(client->cards[c], I_order, o);
                break;
            }
            case MSG_players: {
                LOGD("Client: Recieved players\n");
                const uint8_t* r = data;
                uint8_t np = r_u8(&r);
                inkd_rebuild_begin(source, I_players);
                ink_query q;
                ink_query_init(&q);
                for (uint8_t p=0; p<np; p++) {
                    ink_query_int(&q, I_id, p);
                    inkd d = ink_query_get(&q, source, I_players);
                    inkd_charp(d, I_name, r_str(&r));
                    inkd_bool(d, I_connected, r_u8(&r));
                }
                ink_query_deinit(&q);
                inkd_rebuild_end(source, I_players);
                break;
            }
            default:
                LOGD("Client: Unexpected message type: %d\n", 
                        event.packet->data[0]);
                break;
            }
            enet_packet_destroy(event.packet);
            break;
        }
        case ENET_EVENT_TYPE_DISCONNECT:
        case ENET_EVENT_TYPE_DISCONNECT_TIMEOUT:
            LOGD("Client: Disconnect/timeout\n");
            inkd_trigger(source, I_quit_game);
            break;
        case ENET_EVENT_TYPE_NONE:
            break;
        }
    }
    struct ink_data_event ev;
    while (inkd_listener_poll(client->listener, &ev)) {
        switch (ev.type) {
        case INK_DATA_EVENT_IMPULSE:
            switch (ev.field) {
            case I_draw: {
                inkd data = ink_data_event_get_meta_hold_EXP(&ev, ink_ident_hash("drag_target"));
                if (inkd_get_int(data, I_hand) == inkd_get_int(ev.data, I_hand)) {
                    LOGD("Same hand; ignoring draw event\n");
                    break;
                }
                LOGD("Drawing card\n");
                uint16_t id = inkd_get_int(ev.data, I_id);
                int32_t order = inkd_get_int(ev.data, I_order);
                uint8_t hand = inkd_get_int(source, I_player_id);
                bool played = false;
                send_card_change(client->peer, id, hand, played, order);
                break;
            }
            case I_play: {
                inkd data = ink_data_event_get_meta_hold_EXP(&ev, ink_ident_hash("drag_target"));
                if (inkd_get_int(data, I_hand) == inkd_get_int(ev.data, I_hand)) {
                    LOGD("Same hand; ignoring\n");
                    break;
                }
                LOGD("Playing card.\n");
                uint16_t id = inkd_get_int(ev.data, I_id);
                uint8_t type = inkd_get_int(ev.data, I_type) - 1;
                bool played;
                uint8_t hand;
                switch (client->card_types[type].action) {
                    case ACTION_DISCARD:
                        hand = BANK_HAND;
                        //played = client->deck_types[client->card_types[type].deck].has_discard;
                        played = true;
                        break;
                    case ACTION_REVEAL:
                        hand = inkd_get_int(source, I_player_id);
                        played = true;
                        break;
                    case ACTION_TOP_DRAW:
                        hand = BANK_HAND;
                        played = false;
                        break;
                    default:
                        LOGE("Unexpected action type %d\n",
                                client->card_types[type].action);
                        assert(false);
                        break;
                }
                int32_t order = 1000000000;
                arrforeachv(inkd d, client->cards) {
                    if (inkd_get_int(d, I_deck)-1 == client->card_types[type].deck
                            && inkd_get_bool(d, I_played) == played
                            && inkd_get_int(d, I_order) < order) {
                        order = inkd_get_int(d, I_order);
                    }
                }
                order -= 1;
                LOGD("Card is played: %s\n", played ? "true" : "false");
                send_card_change(client->peer, id, hand, played, order);
                break;
            }
            case I_reconnect:
                client_reconnect(client);
                break;
            case I_shuffle:
                LOGD("Shuffling\n");
                //srand(time(0));
                arrforeachv(inkd c, client->cards) {
                    if (inkd_get_int(c, I_hand) == inkd_get_int(source, I_player_id)) {
                        inkd_int(c, I_local_order, rand());
                    }
                }
            default:
                break;
            }
        default:
            break;
        }
        //break;
    }
    uint8_t player = inkd_get_int(source, I_player_id); 
    arrforeachv(inkd d, client->cards) {
        uint8_t visible = get_card_reveal_flags(player,
                inkd_get_int(d, I_hand),
                inkd_get_bool(d, I_played));
        uint8_t type = inkd_get_int(d, I_type) - 1;
        visible &= client->deck_types[client->card_types[type].deck].visibility;

        inkd_bool(d, I_face_up, visible);
    }
}
