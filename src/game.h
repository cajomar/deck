#ifndef __DECK_GAME__
#define __DECK_GAME__

#include <stdint.h>

#ifdef __ANDROID__
#include <android/log.h>
#define  LOG_TAG "deck"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define  LOGV(...)  __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#else
#include <stdio.h>
#define LOGD(...) printf(__VA_ARGS__)
#define LOGV(...) printf(__VA_ARGS__)
#define LOGI(...) printf(__VA_ARGS__)
#define LOGE(...) fprintf(stderr, __VA_ARGS__)
#endif

#include "client.h"
#include "host.h"
#include <ink_data.h>

typedef struct game {
    struct ink* ink;
    Client* client;
    Host* host;
    inkd_listener* listener;
} Game;

Game* game_new(struct ink* ink);
void game_destroy(Game* g);
void game_step(Game* g);

#endif
