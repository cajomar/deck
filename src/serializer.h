#ifndef SERIALIZER_H_65UCMIKN
#define SERIALIZER_H_65UCMIKN

// Define SERIALIZER_IMPLEMENTATION in *one* C/C++ source file before
// including this file. You can also define
// SERIALIZER_USE_HOST_BYTE_ORDER to avoid converting to/from big/little endian

// TODO decide if I need signed and unsigned versions for each bit width.

#include <stdio.h>
#include <stdint.h>

struct i_writer {
    uint8_t* data;
};

// These functions read values from a buffer of bytes
uint8_t  r_u8 (const uint8_t** r);
 int8_t  r_i8 (const uint8_t** r);
uint16_t r_u16(const uint8_t** r);
 int16_t r_i16(const uint8_t** r);
uint32_t r_u32(const uint8_t** r);
 int32_t r_i32(const uint8_t** r);
uint64_t r_u64(const uint8_t** r);
 int64_t r_i64(const uint8_t** r);
// Return a null-terminated string allocated with malloc
// Will return NULL on failure
char*    r_str(const uint8_t** r);
// Fill `out` with no more than `max_out`-1 bytes with null terminator
void r_strn(const uint8_t** r, char* out, uint32_t max_out);
// These functions append the arguments to w->data
void w_u8 (struct i_writer* w, uint8_t  v);
void w_i8 (struct i_writer* w,  int8_t  v);
void w_u16(struct i_writer* w, uint16_t v);
void w_i16(struct i_writer* w,  int16_t v);
void w_u32(struct i_writer* w, uint32_t v);
void w_i32(struct i_writer* w,  int32_t v);
void w_u64(struct i_writer* w, uint64_t v);
void w_i64(struct i_writer* w,  int64_t v);
// Write a null-terminated string to `w->data`
void w_str(struct i_writer* w, const char* v);
// Write `len` bytes from `v` to `w->data`. `v` need not be null terminated
void w_strn(struct i_writer* w, const char* v, uint32_t len);
// These functions read values from a file stream
uint8_t  rfp_u8 (FILE* fp);
 int8_t  rfp_i8 (FILE* fp);
uint16_t rfp_u16(FILE* fp);
 int16_t rfp_i16(FILE* fp);
uint32_t rfp_u32(FILE* fp);
 int32_t rfp_i32(FILE* fp);
uint64_t rfp_u64(FILE* fp);
 int64_t rfp_i64(FILE* fp);
// Return a string null-terminated allocated with malloc
char* rfp_str(FILE* fp);
// Fill `out` with at most `max_out`-1 bytes with null terminator
void rfp_strn(FILE* fp, char* out, uint32_t max_out);
// These functions write values to a file
void wfp_u8 (FILE* fp, uint8_t  v);
void wfp_i8 (FILE* fp,  int8_t  v);
void wfp_u16(FILE* fp, uint16_t v);
void wfp_i16(FILE* fp,  int16_t v);
void wfp_u32(FILE* fp, uint32_t v);
void wfp_i32(FILE* fp,  int32_t v);
void wfp_u64(FILE* fp, uint64_t v);
void wfp_i64(FILE* fp,  int64_t v);
// Write a null-terminated string to a file stream
void wfp_str(FILE* fp, const char* v);
// Write `len` bytes from `v` to a file streem. `v` need not be null terminated
void wfp_strn(FILE* fp, const char* v, uint32_t len);

#endif /* SERIALIZER_H_65UCMIKN */


#ifdef SERIALIZER_IMPLEMENTATION
#ifndef SERIALIZER_IMPLEMENTATION_ONCE
#define SERIALIZER_IMPLEMENTATION_ONCE

#ifdef SERIALIZER_USE_HOST_BYTE_ORDER
// Use hosts's byte order

#define htn16(x) (x)
#define htn32(x) (x)
#define htn64(x) (x)

#define nth16(x) (x)
#define nth32(x) (x)
#define nth64(x) (x)

#else
// Use network byte order (big endian)
// TODO make sure this is cross platform
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#include <endian.h>

#define htn16(x) htobe16(x)
#define htn32(x) htobe32(x)
#define htn64(x) htobe64(x)

#define nth16(x) be16toh(x)
#define nth32(x) be32toh(x)
#define nth64(x) be64toh(x)

#endif

#include <stdlib.h>

#include "stb_ds.h"
uint8_t  r_u8 (const uint8_t** r) { return *((*r)++); }
 int8_t  r_i8 (const uint8_t** r) { return *((*r)++); }
uint16_t r_u16(const uint8_t** r) { uint16_t i; memcpy(&i, *r, 2); *r += 2; return nth16(i); }
 int16_t r_i16(const uint8_t** r) {  int16_t i; memcpy(&i, *r, 2); *r += 2; return nth16(i); }
uint32_t r_u32(const uint8_t** r) { uint32_t i; memcpy(&i, *r, 4); *r += 4; return nth32(i); }
 int32_t r_i32(const uint8_t** r) {  int32_t i; memcpy(&i, *r, 4); *r += 4; return nth32(i); }
uint64_t r_u64(const uint8_t** r) { uint64_t i; memcpy(&i, *r, 8); *r += 8; return nth64(i); }
 int64_t r_i64(const uint8_t** r) {  int64_t i; memcpy(&i, *r, 8); *r += 8; return nth64(i); }
char*    r_str(const uint8_t** r) {
    uint32_t len = r_u32(r);
    // Do this be sure that malloc will give the right ammount or fail
    if (len+1 == 0) return 0;
    char* s = malloc(len + 1);
    if (!s) return 0;
    s[len] = '\0';
    memcpy(s, *r, len);
    *r += len;
    return s;
}
void r_strn(const uint8_t** r, char* out, uint32_t max_out) {
    uint32_t len = r_u32(r);
    uint32_t count = len && max_out ? (len >= max_out ? max_out-1 : len) : 0;
    memcpy(out, *r, count);
    out[count] = '\0';
    *r += len;
}
void w_u8 (struct i_writer* w, uint8_t  v) { arrput(w->data, v); }
void w_i8 (struct i_writer* w,  int8_t  v) { arrput(w->data, v); }
void w_u16(struct i_writer* w, uint16_t v) { v = htn16(v); memcpy(arraddnptr(w->data, 2), &v, 2); }
void w_i16(struct i_writer* w,  int16_t v) { v = htn16(v); memcpy(arraddnptr(w->data, 2), &v, 2); }
void w_u32(struct i_writer* w, uint32_t v) { v = htn32(v); memcpy(arraddnptr(w->data, 4), &v, 4); }
void w_i32(struct i_writer* w,  int32_t v) { v = htn32(v); memcpy(arraddnptr(w->data, 4), &v, 4); }
void w_u64(struct i_writer* w, uint64_t v) { v = htn64(v); memcpy(arraddnptr(w->data, 8), &v, 8); }
void w_i64(struct i_writer* w,  int64_t v) { v = htn64(v); memcpy(arraddnptr(w->data, 8), &v, 8); }
void w_str(struct i_writer* w, const char* v) {
    if (v) {
        uint32_t len = strlen(v);
        w_u32(w, len);
        memcpy(arraddnptr(w->data, len), v, len);
    } else {
        w_u32(w, 0);
    }
}
void w_strn(struct i_writer* w, const char* v, uint32_t len) {
    if (v && len) {
        w_u32(w, len);
        memcpy(arraddnptr(w->data, len), v, len);
    } else {
        w_u32(w, 0);
    }
}
uint8_t  rfp_u8 (FILE* fp) { uint8_t  i; fread(&i, 1, 1, fp); return i; }
 int8_t  rfp_i8 (FILE* fp) {  int8_t  i; fread(&i, 1, 1, fp); return i; }
uint16_t rfp_u16(FILE* fp) { uint16_t i; fread(&i, 2, 1, fp); return nth16(i); }
 int16_t rfp_i16(FILE* fp) {  int16_t i; fread(&i, 2, 1, fp); return nth16(i); }
uint32_t rfp_u32(FILE* fp) { uint32_t i; fread(&i, 4, 1, fp); return nth32(i); }
 int32_t rfp_i32(FILE* fp) {  int32_t i; fread(&i, 4, 1, fp); return nth32(i); }
uint64_t rfp_u64(FILE* fp) { uint64_t i; fread(&i, 8, 1, fp); return nth64(i); }
 int64_t rfp_i64(FILE* fp) {  int64_t i; fread(&i, 8, 1, fp); return nth64(i); }
char* rfp_str(FILE* fp) {
    uint32_t len = rfp_u32(fp);
    // Do this be sure that malloc will give the right ammount or fail
    if (len+1 == 0) return 0;
    char* s = malloc(len + 1);
    if (!s) return 0;
    s[len] = '\0';
    fread(s, len, 1, fp);
    return s;
}
void rfp_strn(FILE* fp, char* out, uint32_t max_out) {
    uint32_t len = rfp_u32(fp);
    
    uint32_t count = len && max_out ? (len >= max_out ? max_out-1 : len) : 0;
    count = fread(out, 1, count, fp);
    out[count] = '\0';
}
void wfp_u8 (FILE* fp, uint8_t  v) { fwrite(&v, 1, 1, fp); }
void wfp_i8 (FILE* fp,  int8_t  v) { fwrite(&v, 1, 1, fp); }
void wfp_u16(FILE* fp, uint16_t v) { v = htn16(v); fwrite(&v, 2, 1, fp); }
void wfp_i16(FILE* fp,  int16_t v) { v = htn16(v); fwrite(&v, 2, 1, fp); }
void wfp_u32(FILE* fp, uint32_t v) { v = htn32(v); fwrite(&v, 4, 1, fp); }
void wfp_i32(FILE* fp,  int32_t v) { v = htn32(v); fwrite(&v, 4, 1, fp); }
void wfp_u64(FILE* fp, uint64_t v) { v = htn64(v); fwrite(&v, 8, 1, fp); }
void wfp_i64(FILE* fp,  int64_t v) { v = htn64(v); fwrite(&v, 8, 1, fp); }
void wfp_str(FILE* fp, const char* v) {
    if (v) {
        uint32_t len = strlen(v);
        wfp_u32(fp, len);
        fwrite(v, 1, len, fp);
    } else {
        wfp_u32(fp, 0);
    }
}
void wfp_strn(FILE* fp, const char* v, uint32_t len) {
    if (v && len) {
        wfp_u32(fp, len);
        fwrite(v, 1, len, fp);
    } else {
        wfp_u32(fp, 0);
    }
}

#endif /* SERIALIZER_IMPLEMENTATION_ONCE */
#endif /* SERIALIZER_IMPLEMENTATION */
