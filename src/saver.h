#ifndef __DECK_SAVER__
#define __DECK_SAVER__

#include "host.h"
#include "serializer.h"

uint8_t* saver_serialize_cards(Card* cards);
void saver_card(struct i_writer* w, const Card* c);

#endif // __DECK_SAVER__
