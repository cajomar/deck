#include "host.h"

#include "game.h" // For LOGD
#include "saver.h"
#include "stb_ds_loop.h"

#include <assert.h>
#include <stdbool.h>
#include <string.h>

static void host_command(Host* host, enum message_type msg_type) {
    size_t count = arrlen(host->cmd_writer.data);
    LOGD("Server: Sending message %d size %zu\n", msg_type, count);
    ENetPacket* packet = enet_packet_create(NULL,
            1 + count,
            ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = msg_type;
    memcpy((uint8_t*)packet->data+1, host->cmd_writer.data, count);
    enet_host_broadcast(host->server, 0, packet);
    LOGD("Server: Cleaning up...\n");
    arrclear(host->cmd_writer.data);
    LOGD("Server: Done\n");
}

static Card* load_pack(const char* file) {
    srand(time(0));
    Card* cards = 0;

    FILE* fp = fopen(file, "rb");
    if (!fp) {
        LOGE("Server: Couldn't open file %s\n", file);
        return cards;
    }

    // Skip the name
    fseek(fp, rfp_i32(fp), SEEK_CUR);

    uint8_t num_card_types = rfp_u8(fp);

    for (int i=0; i<num_card_types; i++) {
        uint8_t num_cards_in_type = rfp_u8(fp);
        Card* c = arraddnptr(cards, num_cards_in_type);
        for (int j=0; j<num_cards_in_type; j++, c++) {
            c->type = i;
            c->hand = BANK_HAND;
            c->played = false;
            c->order = rand();
        }
        // Skip the name and color
        fseek(fp, rfp_i32(fp) + 4, SEEK_CUR);
        // Skip the image, deck, and action
        fseek(fp, rfp_u64(fp) + 1 + 1, SEEK_CUR);
    }
    fclose(fp);

    return cards;
}

Host* host_create(void) {
    Host* host = calloc(1, sizeof(Host));

    host->address.host = ENET_HOST_ANY;
    host->address.port = NET_PORT;

    host->server = enet_host_create(&host->address, 32, 2, 0, 0);
    if (!host->server) {
        LOGE("Server: Failed to create an ENet server host\n");
    }

    strcpy(host->pack_file_name, "packs/out.pack");
    host->cards = load_pack(host->pack_file_name);
    host->players = 0;
    return host;
}

void host_destroy(Host* host) {
    LOGD("Server: destroying %p\n", host);
    if (!host) return;
    enet_host_destroy(host->server);
    arrfree(host->cmd_writer.data);
    arrfree(host->cards);
    for (size_t i=0; i<arrlenu(host->players); i++) {
        free(host->players[i].name);
    }
    arrfree(host->players);
    free(host);
}

static int connect_new_player(Host* host, const char* name) {
    LOGD("Server: Connecting player %s\n", name);
    arrforeachp(Player* p, host->players) {
        if (strcmp(name, p->name) == 0 && !p->connected) {
            p->connected = true;
            return arr_counter;
        }
    }
    Player* p = arraddnptr(host->players, 1);
    p->connected = true;
    p->name = malloc(strlen(name)+1);
    strcpy(p->name, name);
    return arrlen(host->players)-1;
}

static void send_cards(Host* host) {
    LOGD("Server: Broadcasting cards\n");
    uint8_t* data = saver_serialize_cards(host->cards);
    size_t count = arrlen(data);
    ENetPacket* packet = enet_packet_create(NULL, 1 + count, ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_cards;
    memcpy(packet->data+1, data, count);
    enet_host_broadcast(host->server, 0, packet);
    arrfree(data);
}

static void send_players(Host* host) {
    w_u8(&host->cmd_writer, arrlen(host->players));
    arrforeachp(Player* p, host->players) {
        w_str(&host->cmd_writer, p->name);
        w_u8(&host->cmd_writer, p->connected);
    }
    host_command(host, MSG_players);
}

static void send_assignment_info(ENetPeer* peer, int player) {
    ENetPacket* packet = enet_packet_create(NULL, 1 + 1, ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_player_id;
    packet->data[1] = player;
    enet_peer_send(peer, 0, packet);
}
static void send_pack(Host* host, ENetPeer* peer) {
    // Send pack
    FILE* fp = fopen(host->pack_file_name, "rb");
    fseek(fp, 0, SEEK_END);
    size_t len = ftell(fp);
    rewind(fp);
    ENetPacket* packet = enet_packet_create(NULL, 1 + len,
            ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_pack;
    fread(packet->data + 1, 1, len, fp);
    fclose(fp);
    enet_peer_send(peer, 0, packet);
    const uint8_t* r = packet->data + 1;
    char* n = r_str(&r);
    uint8_t nc = r_u8(&r);
    LOGD("Server: sending pack of size %zu, containing '%s', with %d types of cards\n", len, n, nc);
}

static void recieve_message(Host* host, ENetEvent* event) {
    LOGD("Server: Reading message\n");
    const uint8_t* data = event->packet->data+1;
    LOGD("Server: got message type %d\n", (enum message_type) event->packet->data[0]);
    switch ((enum message_type) event->packet->data[0]) {
        case MSG_player_name: {
            int player = connect_new_player(host, (const char*) data);
            LOGD("Server: new player: %s (id: %d)\n",
                    host->players[player].name,
                    player);
            event->peer->data = (void*)(size_t)player;
            send_assignment_info(event->peer, player);
            send_pack(host, event->peer);
            send_cards(host);
            send_players(host);
            break;
        }
        case MSG_card_change: {
                const uint8_t* r = data;
                const uint16_t c = r_u16(&r);
                LOGD("Server: Card %d changed.\n", c);
                uint8_t h = r_u8(&r);
                bool p = r_u8(&r);
                int32_t o = r_i32(&r);
                host->cards[c].hand = h;
                host->cards[c].played = p;
                host->cards[c].order = o;
                w_u16(&host->cmd_writer, c);
                w_u8(&host->cmd_writer, h);
                w_u8(&host->cmd_writer, p);
                w_i32(&host->cmd_writer, o);
                host_command(host, MSG_card_change);
            }
            break;
        default:
            LOGD("Server got unexpected message type: %d\n", event->packet->data[0]);
            break;
    }
    enet_packet_destroy(event->packet);
}

void host_step(Host* host) {
    if (host == 0) return;
    if (host->server == 0) return;
    ENetEvent event;
    while (enet_host_service(host->server, &event, 0)) {
        LOGD("Recieved event type %d\n", event.type);
        switch (event.type) {
            case ENET_EVENT_TYPE_CONNECT: {
                char ip6[INET_ADDRSTRLEN];
                inet_ntop(AF_INET6, &event.peer->address.host.s6_addr, ip6, INET6_ADDRSTRLEN);
                LOGD("Server: new connection from [%s]:%u\n", ip6, event.peer->address.port);
                break;
            }
            case ENET_EVENT_TYPE_RECEIVE:
                LOGD("Server: Received packet\n");
                recieve_message(host, &event);
                break;
            case ENET_EVENT_TYPE_DISCONNECT_TIMEOUT:
                LOGD("Server: Somebody timed out\n");
                break;
            case ENET_EVENT_TYPE_DISCONNECT: {
                int player_id = (int)(size_t)event.peer->data;
                host->players[player_id].connected = false;
                send_players(host);
                LOGD("Server: Player %d disconnected\n", player_id);
                event.peer->data = 0;
                break;
            }
            case ENET_EVENT_TYPE_NONE:
                break;
        }
    }
}
