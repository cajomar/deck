#ifndef INK_IDENTS_GEN
#define INK_IDENTS_GEN
#define I_bank 6305763429184024921llu // bank
#define I_card_types 388556452271224153llu // card_types
#define I_cards 7620576327682174051llu // cards
#define I_client 9656491785202993132llu // client
#define I_color 15374864329537573005llu // color
#define I_connect_address 6938705967448304889llu // connect_address
#define I_connected 17129855073848772094llu // connected
#define I_deck 2763092244808229906llu // deck
#define I_decks 9190490944877107616llu // decks
#define I_do_spinner 9116851693177737977llu // do_spinner
#define I_draw 12138718136671878972llu // draw
#define I_face_up 11329046078832787314llu // face_up
#define I_game 245837302133227191llu // game
#define I_group 17043716655922593060llu // group
#define I_hand 10742193337690296696llu // hand
#define I_host 16128770115238119356llu // host
#define I_host_address 7511124997009486941llu // host_address
#define I_id 8488222701854180970llu // id
#define I_image 12141535973399564033llu // image
#define I_list 7664278100358914395llu // list
#define I_local_order 16825977290664988020llu // local_order
#define I_main 6571252440867548112llu // main
#define I_name 5668718655922736026llu // name
#define I_play 9516437984556960999llu // play
#define I_played 5397246563052617219llu // played
#define I_player_id 16491757285181836232llu // player_id
#define I_players 2407571527310747085llu // players
#define I_quit_game 13902977106854943141llu // quit_game
#define I_reconnect 12002823787457380996llu // reconnect
#define I_save 17003724860630237142llu // save
#define I_selectSingle 15888457144074405084llu // selectSingle
#define I_shuffle 3217883329980537519llu // shuffle
#define I_slot 5027609672776833817llu // slot
#define I_type 2933420586767109133llu // type
#endif
