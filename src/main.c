#include "game.h"

#include <ink.h>
#include <ink_runner.h>

#include <netdb.h>
#include <stdio.h>
#include <unistd.h>

static Game* g;

void post_step(float dt) {
    game_step(g);
}

int main(void) {
    struct ink* ink = ink_new(0);
    g = game_new(ink);

#ifndef __EMSCRIPTEN__
    for (int port=5555; port<5575; port++) {
        char port_str[13] = "tcp://*:";
        sprintf(port_str+8, "%04d", port);
        if (ink_net_listen(g->ink, port_str))  {
            printf("Listening tcp on port %04d\n", port);
            break;
        }
    }
#endif
    struct ink_runner_options ops = {
        .window_width = 700,
        .window_height = 1200,
        .post_step=post_step,
        .continuous_step=true,
    };
    ink_runner(&ops);

    game_destroy(g);
    return 0;
}
