#ifndef __DECK_CLIENT__
#define __DECK_CLIENT__

#include "enet.h"
#include <ink_data.h>

enum message_type {
    MSG_player_name=0,
    MSG_player_id,
    MSG_cards,
    MSG_card_change,
    MSG_players,
    MSG_pack,
};


enum action {
    ACTION_DISCARD=0,
    ACTION_REVEAL,
    ACTION_TOP_DRAW,
    ACTION_BOTTOM_DRAW,
    ACTION_END,
};

enum reveal {
    REVEAL_IN_HAND = 1,
    REVEAL_IN_DECK_DRAW = 2,
    REVEAL_IN_DECK_PLAYED = 4,
    REVEAL_IN_OPPONENT_DRAW = 8,
    REVEAL_IN_OPPONENT_PLAYED = 16,
    REVEAL_IN_DRAW = 11,
    REVEAL_WHEN_PLAYED = 21,
};

typedef struct card_type {
    uint8_t action;
    uint8_t deck;
} CardType;

typedef struct deck_type {
    uint8_t visibility;
    //uint8_t has_discard;
} DeckType;

typedef struct client {
    ENetHost* client;
    ENetPeer* peer;
    char* player_name;
    char* host_address;
    inkd_listener* listener;
    inkd* cards; // stretchy_buffer
    CardType* card_types; // stretchy_buffer
    DeckType* deck_types; // stretchy_buffer
} Client;

Client* client_create(struct ink* ink, const char* host, const char* name);
void client_destroy(Client* client);
void client_step(Client* client, struct ink* ink);

#endif //  __DECK_CLIENT__
