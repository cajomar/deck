#include "saver.h"

#include "serializer.h"
#include "stb_ds_loop.h"


uint8_t* saver_serialize_cards(Card* cards) {
    struct i_writer w = { .data=0 };

    w_u16(&w, arrlenu(cards));
    arrforeachp(Card* c, cards) {
        saver_card(&w, c);
    }
    return w.data;
}

void saver_card(struct i_writer* w, const Card* c) {
    w_u8(w, c->type);
    w_u8(w, c->hand);
    w_u8(w, c->played);
    w_i32(w, c->order);
}
