#ifndef __DECK_LOADER__
#define __DECK_LOADER__

#include <stdlib.h>
#include "host.h"

Card* loader_deserialize(const uint8_t** r);


#endif // __DECK_LOADER__
